import React, { Component } from "react";
class List extends Component{

  constructor() {
    super();
    this.state = { data: [] };
  }
    async componentDidMount() {
        const response = await fetch(
          `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?cards=all&card_fields=all&filter=open&fields=all&key=${this.props.keys}&token=${this.props.tokens}`
        );
        const json = await response.json();
        this.setState({ data: json });
      }    


      render(){
        return(
          <div className='lists'>
            
              {this.state.data.map((list)=>(
                <div className='list'>
                  {list.name}
                  {list.cards.map(card=>(
                    <div className='card'>
                    {card.name}
                    </div>
                  ))}
                  
                  </div>

                
              
              ))
            }
          </div>
        );
      }
}

export default List;