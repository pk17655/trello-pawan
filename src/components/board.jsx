import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
// import ReactDOM from 'react-dom';
import { Link } from "react-router-dom";
import List from './List'
const credential = {
  key: "4922d8cb43c158cd71bd4042c7ae3cf5",
  token: "62f7810750c045a21c8117b6da1cae192448c23eb470c00cddaf99ec5fb0279d",
  username: "pawankumar280"
};

class Board extends Component {
  constructor() {
    super();
    this.state = { data: [] };
  }

  async componentDidMount() {
    const response = await fetch(
      `https://api.trello.com/1/members/${credential.username}/boards?filter=all&fields=all&lists=none&memberships=none&organization=false&organization_fields=name%2CdisplayName&key=${credential.key}&token=${credential.token}`
    );
    const json = await response.json();
    this.setState({ data: json });
  }
  render() {
    return (
      <Router>
        <Route
          exact
          path="/"
          render={props => (
            <div className="boards">
              {this.state.data.map(board => (
          

                <Link to={`b/${board.id}/${board.name}`}>
                <div key={board.id} className="board-name">
                   {board.name} </div> </Link>
           
                
              ))}
            </div>
          )}
        />
        <Route path='/b/:id/:boardName' render={props =>(
            <List {...props} keys={credential.key} tokens={credential.token}/>
        )}
        />
      </Router>
    );
  }
}

export default Board;
